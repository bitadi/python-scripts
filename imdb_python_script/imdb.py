#! /usr/bin/env python
import sys
import os
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import strftime,localtime
import re
from bs4 import BeautifulSoup
import requests

class Logger():
	def __init__(self,filename="default.log"):
		self.terminal=sys.stdout
		self.log=open(filename,"a")
	def write(self,message):
		self.terminal.write(message)
		self.log.write(message)


def checkPresence(path):
    if os.path.exists(path)==True:
        return True
    else:
        return False

def makeFolder(path):
    if checkPresence(path)==False:
        os.mkdir(path)


movieFormat=['.avi','.mkv','.mp4','.divx']
def trimName(stg):
	r = re.compile("(([a-zA-Z])+.)*")
	m=r.match(stg)
	m=m.group()
	if m.endswith("."):
				m=m.strip(".")
	return m

def checkForMovie(name):
	for format in movieFormat:
		if name.endswith(format):
			name=name.strip("."+format)

			return name
	return False	
		


'''
def startParsinMovieFolder(fixedPath):
	movieList=os.listdir(fixedPath)
	#print movieList
	for movie in movieList:
		newpath=fixedPath+"/"+movie
		#print newpath
		if(os.path.isdir(newpath)==True):
            		startParsinMovieFolder(newpath)
        else:
        	
        	#if checkForMovie(movie)==True:
        		#mov=trimName(movie)
        		print movie
'''

#def searchGoogleReturnImdbLink(movieName):
#def imdbSearch():

def stripRating(stg):

	stg=stg.strip(' Rating: ')
	#print "stg here is:",stg
	r=re.compile('([0-9])([.][0-9])*')
	r=r.match(stg)
	#print r,"-----lklknklnklnl---->",r.group
	r=r.group()
	return r

def SearchByBeautifulsoup(name):
	r=requests.get("https://www.google.co.in/search?q=imdb+"+name+" +movie")
	soup=BeautifulSoup(r.text)
	try:
	    rat=soup.find("div", class_="slp").text.encode('ascii','ignore')
	    imdbRating=stripRating(rat)
	    return imdbRating
    	except Exception, ex:
    		#print "could not find rating for this movie."
	    	raise Exception('could not find rating for this movie.')



def Search(name):
    browser=webdriver.Firefox()
    browser.get("https://www.google.co.in/search?q=imdb+"+name+" +movie")
    try:
	    csb=browser.find_element_by_css_selector('.slp').text
	    imdbRating=stripRating(csb)
	    return imdbRating
    except Exception, ex:
	    raise Exception('could not find rating for this movie.')
    finally:
 	    browser.close()
	
    

def startParsinMovieFolder(locn):
	for root, dire, files in os.walk(locn):
		for fil in files:
			orignalName=fil
			trimmedName=checkForMovie(fil)
			if trimmedName != False:
        			movie=trimName(trimmedName)
        			#print trimmedName,"------>",movie
        			
        			try:
        				imdbRating=SearchByBeautifulsoup(movie)
        				print orignalName,",",imdbRating
        				#print "renaming "+orignalName+" to ---->"+orignalName+"-Imdb-"+imdbRating
        				#os.rename(orignalName,orignalName+"-Imdb-"+imdbRating)
        			except Exception,ex:
        				print ex,orignalName
        			


def main():
	IMDB_SCRIPT_LOC="/home/adityatomer/Personel/code/imdb_python_script/"
	MOVIE_SEARCH_LOC="/media/adityatomer/My Passport/Movies"
	
	sys.stdout=Logger(IMDB_SCRIPT_LOC+"/imdblog.csv")
	print strftime("%y-%m-%d %H:%M:%S",localtime())
	startParsinMovieFolder(MOVIE_SEARCH_LOC)
	#newdef(MOVIE_SEARCH_LOC)

if __name__=="__main__":
	main()




