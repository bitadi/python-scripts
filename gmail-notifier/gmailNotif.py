#!/usr/bin/env python
import requests
from bs4 import BeautifulSoup
import pynotify
from time import sleep
import os 
import sys
import imaplib
import getpass
import email
import datetime
import subprocess
import personel as personelInfo
NoOfMail=5
MAILBOX='[Gmail]/All Mail'

def sendmessage(title, message):
    cmd='notify-send '+'"'+message+'"'
    subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    

    # pynotify.init("Test")
    # notice = pynotify.Notification(title, message)
    # notice.show()
    return

def saveToFolder(num,data,directory):
    f = open('%s/%s.eml' %(directory, -num), 'wb')
    f.write(data)
    f.close()


def processEmails(mail):
    stgBuild=str()
    print "running script"
    #print mail.list()
    #conn.select(readonly=1) # Select inbox or default namespace
    mail.select(MAILBOX)
    result, data = mail.uid('search', None, 'UNSEEN')
    uid_list = data[0].split()
    stgBuild=str()
    if len(uid_list)>0:
        stgBuild="aditya you have got "+str(len(uid_list))+" new email."
        cmd='''espeak -s 130 '''+'"'+stgBuild+'"'
        p  = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        out,err=p.communicate()
        
        for uid in uid_list:
            st,email_raw=mail.uid("fetch",uid,"(RFC822)")
            for response_part in email_raw:
                if isinstance(response_part, tuple):
                    msg = email.message_from_string(response_part[1])
                    stgBuild+="\nSUBJECT: "+msg["subject"]
                    stgBuild+="\nFROM:"+msg["from"]
                    stgBuild+="\n------------------------"
                    #stgBuild+= '%-8s: %s' % (str(header.upper()), str(msg[header]))
        print stgBuild
        sendmessage("Gmail Notification",stgBuild)



def connectImap():
    M=imaplib.IMAP4_SSL('imap.gmail.com')
    try:
        M.login(personelInfo.getUsername(),personelInfo.getKey())
        processEmails(M)
        
    except Exception,e:#imaplib.IMAP4.error:
        print "sorry buddy...login failed!! try again!",e
    finally:
        M.close()
        M.logout()

connectImap()
